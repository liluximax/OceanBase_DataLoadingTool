# -*- coding: UTF-8 -*-
import traceback, main
import mysql.connector as ob


class ExcuteUtil(object):
    def __init__(self, db_config):
        self.connect = ob.connect(**db_config)
        self.cursor = self.connect.cursor()

    def excute_insert(self, sql):
        try:
            self.cursor.execute(sql)
            self.connect.commit()
        except Exception, e:
            if e.args[0] == 4030:
                print "Over tenant memory limits"
                main.shared.wait = True
            else:
                print e
    def close(self):
        self.connect.close()
