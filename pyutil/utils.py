# -*- coding: UTF-8 -*-
import json, random, time, resolve_utils, re, imp


def readpps(filepath):
    try:
        f = open(filepath, "r")
        values = {}
        for row_value in f.readlines():
            if "=" in row_value:
                row_value = row_value.replace("\n", "")
                key = row_value.split("=")[0]
                value = row_value.split("=")[1]
                values[key] = value.strip()
        f.close()
        return values
    except Exception as ex:
        print(ex)
        return "error"


def readjson(filepath):
    with open(filepath) as f:
        data = json.load(f)
    return data


# 根据类型生成随机值，传入列类型和分区信息
def generate_values(columns, batch, partition, offset, seed):
    value = ""
    index = partition.get('index')
    sub_index = partition.get('sub_index', -1)
    part_num = int(partition.get('part_num'))
    part_type = partition.get('part_type').upper()

    if part_type == 'HASH':
        gap = 100000
        start = random.randint(0, part_num - 1) + offset * gap
        end = gap - 1 + offset * gap

    # 仅限于分区字段为int
    elif part_type == 'RANGE':
        range_info = partition['range']
        part_id = random.randint(0, part_num - 1)
        high_bound_val = int(range_info[part_id]['high_bound_val'])
        low_bound_val = int(range_info[part_id]['low_bound_val'])

    # 仅限于分区字段为TIMESTAMP
    elif part_type == 'RANGE_COLUMNS':
        range_info = partition['range']
        part_id = random.randint(0, part_num - 1)
        high_bound_val_str = range_info[part_id]['high_bound_val']
        low_bound_val_str = range_info[part_id]['low_bound_val']
        # 匹配int型整数
        if partition['part_column_type'] == 'INT':
            high_bound_val = int(high_bound_val_str)
            low_bound_val = int(low_bound_val_str)
        else:
            high_bound_val = time2long(range_info[part_id]['high_bound_val'])
            low_bound_val = time2long(range_info[part_id]['low_bound_val'])

    # 二级分区初始随机值设置
    if partition.has_key('sub_part_type'):
        sub_part_num = int(partition['sub_part_num'])
        sub_part_type = partition['sub_part_type'].upper()

        if sub_part_type == 'HASH':
            gap = 100000
            sub_start = random.randint(0, sub_part_num - 1) + offset * gap
            sub_end = gap - 1 + offset * gap

        elif sub_part_type == 'RANGE_COLUMNS':
            range_info = partition['range']
            sub_part_id = random.randint(0, sub_part_num - 1)
            high_bound_val_str = range_info[sub_part_id]['high_bound_val']
            low_bound_val_str = range_info[sub_part_id]['low_bound_val']
            # 匹配int型整数
            if partition['sub_part_column_type'] == 'INT':
                sub_high_bound_val = int(high_bound_val_str)
                sub_low_bound_val = int(low_bound_val_str)
            else:
                sub_high_bound_val = time2long(range_info[sub_part_id]['high_bound_val'])
                sub_low_bound_val = time2long(range_info[sub_part_id]['low_bound_val'])

    for i in range(batch):
        value += "("
        for i in range(len(columns)):
            column = columns[i]
            column_data_type = column['data_type']
            # 字段值设为定值
            if column.has_key('default'):
                if column_data_type == 'INT':
                    value += str(column['default']) + ','
                else:
                    value += '"' + str(column['default']) + '",'
            # 为一级分区字段赋值
            elif index == i:
                if part_type == "HASH":
                    value += str(random_partition_int_hash(start, end, part_num)) + ","
                elif part_type == "RANGE":
                    if column_data_type == 'INT':
                        value += str(random_partition_int_range(low_bound_val, high_bound_val)) + ","
                    else:
                        print "RANGE分区仅支持INT型分区键"
                elif part_type == "RANGE_COLUMNS":
                    if column_data_type == 'INT':
                        value += str(random_partition_int_range(low_bound_val, high_bound_val)) + ","
                    elif column_data_type == 'DATETIME':
                        value += random_partition_time_rangecolumns(low_bound_val, high_bound_val) + ","
                else:
                    print "分区键字段类型不支持"
            # 为二级分区字段赋值
            elif sub_index == i:
                if sub_part_type == 'HASH':
                    value += str(random_partition_int_hash(sub_start, sub_end, sub_part_num)) + ","
                elif sub_part_type == 'RANGE_COLUMNS':
                    if column_data_type == 'INT':
                        value += str(random_partition_int_range(sub_low_bound_val, sub_high_bound_val)) + ","
                    elif column_data_type == 'DATETIME':
                        value += random_partition_time_rangecolumns(sub_low_bound_val, sub_high_bound_val) + ","
            # 为其余非分区字段赋值
            else:
                if column['is_nullable'] == 1:
                    if is_nullable(seed):
                        value += 'NULL,'
                        continue
                if column_data_type == "INT":
                    value += str(random_int())
                elif column_data_type == "VARCHAR" or column_data_type == 'CHAR':
                    value += random_char(int(column['char_min_length']), int(column['char_max_length']))
                elif column_data_type == 'TIMESTAMP' or column_data_type == 'DATETIME' or column_data_type == 'DATE' or column_data_type == 'YEAR' or column_data_type == 'TIME':
                    value += random_timestamp()
                elif column_data_type == 'DECIMAL':
                    value += random_decimal(int(column['numeric_precision']), int(column['numeric_scale']))
                elif column_data_type == 'DOUBLE' or column_data_type == 'FLOAT':
                    value += str(random_double())
                else:
                    print (column_data_type + " not support ")
                value += ","
        value = value[:-1]
        value += "), "

    return value


alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z", " "]


def random_char(min_length, max_length):
    len = random.randint(min_length, max_length)
    chars = []
    for i in range(len):
        index = random.randint(0, 26)
        chars.append(alphabet[index])
    return "\"" + "".join(chars) + "\""


def random_int():
    return random.randint(-1000, 1000)


def random_double():
    return random.random() * 1000


def random_timestamp():
    t = random.randint(946656000, 1497888000) * 10 ** 6
    return "USEC_TO_TIME(%s)" % (t)


def random_time():
    t = random.randint(946656000, 1497888000)
    return "\"" + time.strftime("%H:%M:%S", time.localtime(t)) + "\""


def random_date():
    t = random.randint(946656000, 1497888000)
    return "\"" + time.strftime("%Y-%m-%d", time.localtime(t)) + "\""


def random_year():
    t = random.randint(1970, 2069)
    return "\"" + str(t) + "\""


def random_partition_int_hash(start, end, num):
    return random.randrange(start, end, num)


def random_partition_int_range(start, end):
    return random.randint(start, end - 1)


def random_partition_time_rangecolumns(start, end):
    t = random.randint(start, end - 1) * 10 ** 6
    return "USEC_TO_TIME(%s)" % (t)


def random_seed(rate):
    seed = [0] * rate + [1] * (100 - rate)
    return seed


def is_nullable(seed):
    index = random.randint(0, 99)
    return (True if seed[index] == 0 else False)


def random_decimal(precision, scale):
    integer = random.randint(0, 10 ** (precision - scale) - 1)
    decimal = random.randint(0, 10 ** scale - 1)
    res = str(integer) + "." + str(decimal)
    return res


def time2long(t):
    imp.acquire_lock()
    st = time.strptime(t, '%Y-%m-%d %H:%M:%S')
    imp.release_lock()
    time_long = time.mktime(st)
    return time_long


def date2long(t):
    st = time.strptime(t, '%Y-%m-%d')
    return int(time.mktime(st))


def year2long(t):
    st = time.strptime(t, '%Y')
    return int(time.mktime(st))
