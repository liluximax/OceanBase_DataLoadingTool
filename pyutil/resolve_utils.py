# -*- coding: UTF-8 -*-
import json, sys
import mysql.connector as ob

reload(sys)
sys.setdefaultencoding('utf8')


# 从配置文件中读取配置信息，从系统表中查询表结构
def query_tb_info(db_config, param):
    tb_info = {}
    test = resolve(db_config, **param)
    res = test.query()
    if res:
        columns, partition = res
        batch = int(param['batch'])
        count = int(param['count'])
        process_num = int(param['process_num'])
        thread_num = int(param['thread_num'])
        nullable_rate = int(param['nullable_rate'])

        tb_info['database'] = param['database']
        tb_info['table'] = param['table']
        tb_info['process_num'] = process_num
        tb_info['thread_num'] = thread_num
        tb_info['count'] = count
        tb_info['batch'] = batch
        tb_info['columns'] = columns
        tb_info['partition'] = partition
        tb_info['nullable_rate'] = nullable_rate

        tb_info_json = json.dumps(tb_info, default=lambda o: o.__dict__, sort_keys=True, indent=4)
        print '------------------------------------------------------------------------------------'
        print "%s: " % (tb_info['table'])
        print tb_info_json
        print '------------------------------------------------------------------------------------\n'
        return tb_info


class resolve(object):
    def __init__(self, db_config, **param):
        self.connect = ob.connect(**db_config)
        self.database = param['database']
        self.table = param['table']
        self.columns_config = param.get('columns')
        self.root_connect = ob.connect(host=db_config['host'], user='root', port=db_config['port'])

    def print_info(self):
        print self.databse, self.table

    def query(self):
        table_id = self.query_for_table_id()
        if table_id:
            columns = self.query_columns()
            partition = self.query_partition(table_id, columns)
            self.close()
            if partition:
                return columns, partition
        else:
            print "\n!!!!!!!!!!!!"
            print "%s.%s not exist, please check" % (self.database, self.table)
            print "!!!!!!!!!!!!\n"
            sys.exit(1)

    def query_columns(self):
        sql = "select ORDINAL_POSITION, COLUMN_NAME, DATA_TYPE, IS_NULLABLE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = \'%s\' and TABLE_NAME = \'%s\'" % (
            self.database, self.table)
        cursor = self.connect.cursor()
        cursor.execute(sql)
        result = cursor.fetchall()
        columns = [None] * cursor.rowcount
        for r in result:
            column = {}
            column['name'] = str(r[1])
            column['data_type'] = str(r[2]).upper()
            column['is_nullable'] = (1 if r[3] == 'YES' else 0)
            column['char_max_length'] = int(r[4])
            column['char_min_length'] = 1
            column['numeric_precision'] = r[5]
            column['numeric_scale'] = r[6]
            index = r[0]
            columns[index - 1] = column

        self.set_columns_config(columns)

        cursor.close()
        return columns

    def set_columns_config(self, columns):
        columns_config = self.columns_config
        if columns_config:
            for c in columns_config:
                c['data_type'] = c['data_type'].upper()
                for o in columns:
                    if o['name'] == c['name'] and o['data_type'] == c['data_type']:
                        if o['is_nullable'] == 0 and c['is_nullable'] == 1:
                            print "[%s] '%s'字段 不可设置为null" % (self.table, str(c['name']))
                            c['is_nullable'] = 0
                        if 'char_max_length' in c:
                            if int(o['char_max_length']) < int(c['char_max_length']):
                                print "[%s] '%s'字段 char_max_length属性值设置过大" % (self.table, str(c['name']))
                                c['char_max_length'] = o['char_max_length']
                        if 'default' in c:
                            # 检查char类型default合法性,如果超长将忽略default
                            if o['data_type'] == 'VARCHAR' or o['data_type'] == 'CHAR':
                                if len(c['default']) > o['char_max_length']:
                                    print "%s default值非法,将忽略" % (o['name'])
                                    del c['default']
                        for (k, v) in c.items():
                            o[k] = v

    def query_partition(self, table_id, columns):
        sql = "select part_expr, part_type, part_num, sub_part_expr, sub_part_type, def_sub_part_num from oceanbase.__all_part_info where table_id = \'%s\'" % (
            table_id)
        cursor = self.root_connect.cursor()
        cursor.execute(sql)
        result = cursor.fetchone()
        if not result:
            print "[%s] 无分区结构，将继续执行插入" % (self.table)
            partition = {}
            partition['part_expr'] = ''
            partition['part_num'] = 0
            partition['part_type'] = ''
            partition['index'] = -1
            return partition

        partition = {}
        partition['part_expr'] = str(result[0])
        partition['part_num'] = result[2]

        index = -1
        for i in range(len(columns)):
            if columns[i]['name'] == partition['part_expr']:
                partition_column_type = columns[i]['data_type'].upper()
                index = i
        if index == -1:
            print "[%s] 一级[不支持]自定义表达式进行分区，将停止插入操作" % (self.table)
            return None

        partition['index'] = index
        partition['part_column_type'] = partition_column_type
        part_type = result[1]

        print "[%s] 一级分区: %s" % (self.table, partition['part_expr'])
        type = ''
        if part_type == 0:
            type = 'hash'
        elif part_type == 3:
            type = 'range'
            partition['range'] = self.query_for_range_part(table_id, partition_column_type)
        elif part_type == 4:
            type = 'range_columns'
            partition['range'] = self.query_for_range_part(table_id, partition_column_type)
        else:
            print "[%s]: [%s]一级分区数据类型不支持" % (self.table, type)
        partition['part_type'] = type

        if str(result[3]):
            partition['sub_part_expr'] = str(result[3])
            partition['sub_index'] = -1
            partition['sub_part_num'] = result[5]
            sub_part_type = result[4]

            # TODO 二级分区信息
            print "[%s] 二级分区: %s" % (self.table, partition['sub_part_expr'])
            sub_index = -1
            for i in range(len(columns)):
                if columns[i]['name'] == partition['sub_part_expr']:
                    sub_partition_column_type = columns[i]['data_type'].upper()
                    sub_index = i
            if sub_index == -1:
                print "[%s] 二级分区[不支持]自定义表达式进行分区，将停止插入操作" % (self.table)
                return None

            partition['sub_index'] = sub_index
            partition['sub_part_column_type'] = sub_partition_column_type

            if sub_part_type == 0:
                type = 'hash'
            elif sub_part_type == 4:
                type = 'range_columns'
                partition['range'] = self.query_for_range_sub_part(table_id, sub_partition_column_type)
            else:
                print "[%s] [%s]二级分区类型不支持" % (self.table, type)
            partition['sub_part_type'] = type

        return partition

    def query_for_database_id(self):
        sql = "select database_id from oceanbase.__all_database where database_name = \'%s\'" % (self.database)
        cursor = self.root_connect.cursor()
        cursor.execute(sql)
        return cursor.fetchone()[0]

    def query_for_table_id(self):
        database_id = self.query_for_database_id()
        sql = "select table_id from oceanbase.__all_table where table_name = \'%s\' and database_id = %s" % (
            self.table, database_id)
        cursor = self.root_connect.cursor()
        cursor.execute(sql)
        result = cursor.fetchone()
        if result:
            return result[0]

    # 查询一级分区为range时，分区信息
    def query_for_range_part(self, table_id, partition_column_type):
        return self.query_for_range_info(table_id, partition_column_type, 0)

    # 查询二级分区为range时，分区信息
    def query_for_range_sub_part(self, table_id, partition_column_type):
        return self.query_for_range_info(table_id, partition_column_type, 1)

    def query_for_range_info(self, table_id, partition_column_type, type):
        if type == 0:
            sql = "select part_id, high_bound_val from oceanbase.__all_part where table_id = %s" % (table_id)
        elif type == 1:
            sql = "select sub_part_id, high_bound_val from oceanbase.__all_sub_part where table_id = %s and part_id = 0" % (
                table_id)
        cursor = self.root_connect.cursor()
        cursor.execute(sql)
        rs = cursor.fetchall()
        l = []
        if partition_column_type == "INT":
            pre = 0
        elif partition_column_type == 'TIMESTAMP' or partition_column_type == 'DATETIME' or partition_column_type == 'DATE':
            pre = "1970-01-01 00:00:00"
        elif partition_column_type == 'DATE':
            pre = '1970-01-1'
        elif partition_column_type == 'YEAR':
            pre = '1970'
        for r in rs:
            part = {}
            cur = r[1].strip("'")
            if cur == 'MAXVALUE':
                if partition_column_type == "INT":
                    cur = 2147483647
                elif partition_column_type == 'TIMESTAMP' or partition_column_type == 'DATETIME' or partition_column_type == 'DATE':
                    cur = '2099-12-31 00:00:00'
                elif partition_column_type == 'YEAR':
                    cur = '2099'
            part['part_id'] = int(r[0])
            part['low_bound_val'] = pre
            part['high_bound_val'] = cur
            l.append(part)
            pre = cur
        return l

    def close(self):
        self.connect.close()
        self.root_connect.close()
