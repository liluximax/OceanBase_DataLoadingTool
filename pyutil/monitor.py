# -*- coding: UTF-8 -*-
import mysql.connector as ob
import time, traceback, main

class Monitor():
    def __init__(self, db_config):
        self.connect = ob.connect(host=db_config['host'], user='root', port=db_config['port'])
        self.cursor = self.connect.cursor()
        self.flag = True

    def task(self):
        print "\nmonitor process start, the threads will stop when ob is merging\n"
        while True:
            # self.manual_merge()
            self.monitor_merge()
            time.sleep(10)
        self.close()

    # 通过监测内存，手动触发merge
    def manual_merge(self):
        if self.flag:
            mem = self.get_mem()
            if mem > 100:
                print "prepare excute manual merge"
                main.shared.wait = True
                time.sleep(3)
                self.excute_merge()
                time.sleep(3)

    def monitor_merge(self):
        st = self.get_merge_status()
        if st == 1:
            main.shared.wait = True
            print "ob is merging"
            # 关闭检查内存，手动触发merge机制
            self.flag = False
        elif st == 0:
            if main.shared.wait:
                # 插入线程恢复工作
                time.sleep(1)
                main.shared.wait = False
                # 内存检查函数恢复工作
                time.sleep(3)
                self.flag = True

    def get_mem(self):
        sql = "select TOTAL as mem from oceanbase.gv$memstore where TENANT_ID = 1"
        try:
            self.cursor.execute(sql)
            mem = self.cursor.fetchone()
            return mem[0] / 1024 / 1024
        except Exception:
            print "\n" + str(traceback.print_exc()) + "\n"

    def get_merge_status(self):
        sql = "select value from oceanbase.__all_zone where name = 'merge_status' and zone = ''"
        try:
            self.cursor.execute(sql)
            status = self.cursor.fetchone()
            return status[0]
        except Exception, e:
            print "check merge_status error [%s]" % (e.args[0])
            print e

    def excute_merge(self):
        sql = "alter system major freeze"
        try:
            self.cursor.execute(sql)
            self.connect.commit()
            self.flag = False
            print "begin excute manual merge"
        except Exception:
            print "\n" + str(traceback.print_exc()) + "\n"

    def close(self):
        print "close ob connect"
        self.connect.close()

if __name__ == '__main__':
    monitor = Monitor()
    # print monitor.get_merge_status()
    monitor.excute_merge()