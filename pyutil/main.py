# -*- coding: UTF-8 -*-
from multiprocessing import Process, Manager
import multiprocessing, produce_insert, time, os, monitor, utils, resolve_utils, json, sys

shared = Manager().Namespace()
shared.wait = False


def table_insert_task(config):
    param = utils.readjson(config)
    tb_info = resolve_utils.query_tb_info(db_config, param)
    if tb_info:
        # 每条insert语句的行数
        batch = int(tb_info['batch'])
        # 每个进程迭代次数
        count = int(tb_info['count'])
        process_num = int(tb_info['process_num'])
        thread_num = int(tb_info['thread_num'])
        table = tb_info['table']

        start = time.time()
        jobs = []
        for i in range(process_num):
            job = produce_insert.ProduceUtil(tb_info, db_config)
            job.start()
            jobs.append(job)

        for p in jobs:
            p.join()

        end = time.time()
        spend = (end - start)
        amount = batch * count * process_num * thread_num
        print "[%s] task use %0.2f s, [%d] insert speed: %d per/second" % (table, spend, amount, amount / spend)


if __name__ == '__main__':

    table_selected = utils.readjson("config/table_select.json")
    table_selected = [str(e) + '.json' for e in table_selected['selected']]
    print "选择%s进行插入" % (table_selected)
    table_configs = ['config/columns_def/' + f for f in os.listdir('config/columns_def') if
                     f.endswith(".json") and f in table_selected]

    if len(table_configs) < 1:
        print "未选择插入任何表,程序将退出"
        sys.exit(1)

    db_properties = "config/db_config/database.properties"
    database = utils.readjson(table_configs[0])['database']
    db_config = utils.readpps(db_properties)
    if database == 'qianji':
        print "使用qianji机器数据源"
        db_config = utils.readpps("config/db_config/test.properties")

    # 开启监控merge状态进程
    monitor = monitor.Monitor(db_config)
    monitor_process = Process(target=monitor.task)
    monitor_process.daemon = True
    monitor_process.start()

    table_process = []
    for config in table_configs:
        p = Process(target=table_insert_task, args=(config,))
        p.start()
        table_process.append(p)

    for p in table_process:
        p.join()

    monitor.close()
