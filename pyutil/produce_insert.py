# -*- coding: UTF-8 -*-
from utils import *
from multiprocessing import Process
from excute_insert import ExcuteUtil
import os, time, multiprocessing, threading, main


class ProduceUtil(Process):
    def __init__(self, tb_info, db_config):
        super(ProduceUtil, self).__init__()
        self.tb_info = tb_info
        self.db_config = db_config
        self.table = tb_info['table']

    def run(self):
        print '[%s]: process[%s] start' % (self.table, os.getpid())
        start = time.time()
        self.multi_task()
        end = time.time()
        print '[%s]: process[%s] use %0.2f seconds' % (self.table, os.getpid(), (end - start))

    # 每个进程下开10个线程
    def multi_task(self):
        thread_num = int(self.tb_info['thread_num'])
        tasks = []
        for i in range(thread_num):
            thread_name = str(os.getpid()) + "|" + str(i)
            t = threading.Thread(target=self.produce_sql, name=thread_name)
            tasks.append(t)
            t.start()
        for t in tasks:
            t.join()

    def produce_sql(self):
        # print "[%s]: thread[%s] start" % (self.table, threading.current_thread().getName())
        tb_info = self.tb_info
        util = ExcuteUtil(self.db_config)
        sql = "INSERT INTO DATABASE.TABLE "
        database = tb_info['database']
        table = tb_info['table']
        columns = tb_info['columns']
        partition = tb_info['partition']
        batch = int(tb_info['batch'])
        count = int(tb_info['count'])
        seed = random_seed(tb_info['nullable_rate'])

        sql = sql.replace("DATABASE", database).replace("TABLE", table)
        sql += "("
        for i in range(len(columns)):
            column = columns[i]
            sql += column['name'] + ", "
        sql = sql[:-2]
        sql += ") VALUES "

        # 线程暂停与恢复标志位
        wait = threading.Event()
        wait.set()
        offset = 0
        # 迭代count轮
        for i in range(count):
            if main.shared.wait:
                wait.clear()
                print "[%s]: [%s]线程暂停" % (self.table, threading.current_thread().getName())
                while main.shared.wait:
                    time.sleep(1)
                wait.set()
                offset = offset + 1
                print "[%s]: [%s]线程恢复 此时迭代到[%s]轮, offset[%s]" % (self.table, threading.current_thread().getName(), str(i), str(offset))
            else:
                sql_copy = sql[:]
                values = generate_values(columns, batch, partition, offset, seed)
                sql_copy += values
                sql_copy = sql_copy[:-2]
                sql_copy += ";"
                # print sql_copy
                util.excute_insert(sql_copy)
        util.close()
