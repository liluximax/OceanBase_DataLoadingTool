# OceanBase数据导入工具Python版
## 基本功能
* 根据配置文件和系统表中的信息，按指定规则生成随机内容，对指定表并发执行数据装载，
* 为了提升性能，确保每条INERT语句插入到同一分区中
* 插入过程中当监控进程检测到OceanBase处于Merging状态时候，暂停所有工作线程，直到内存中所有的数据落盘到硬盘后，工作线程恢复工作直至完成任务


## 环境需求
* Python2.7
* MySQL-connector-python

**安装教程:**

```
1. tar -xzvf mysql-connector-python-2.1.6.tar.gz
2. cd mysql-connector-python-2.1.6
3. sudo python setup.py install

验证是否安装成功
1.输入python，进入交互界面
2.import mysql.connector，如果没有任何报错信息，则安装MySQL-connector-python包成功

```
## 使用文档

1. 运行`python main.py`启动程序，所有的配置文件位于`config/`文件夹中
2. `db_config/`下存放数据库连接信息，将连接信息写入到`database.properties`文件中
3. `table_select.json`文件中配置需要执行插入的表名（可配置成同时对多张表进行插入）
4. `columns_def/`下存放对字段随机值有特殊需求的配置信息，命名规则是`tablename.json`
5. 配置文件均为json格式，请确保json格式正确
6. 请确保数据库中已经创建目标database和table

**Example:**

```
{
  "database": "qianji",
  "table": "sub_part",
  "batch": 50,
  "count": 500,
  "process_num": 10,
  "thread_num": 10,
  "nullable_rate": 30,
  "columns": [
    {
      "name": "info",
      "data_type": "varchar",
      "is_nullable": 1,
      "char_max_length": 20,
      "char_min_length": 5,
      "default": "aaaa"
    },
    {
      "name": "user_id",
      "data_type": "int",
      "is_nullable": 1,
      "char_max_length": 10,
      "char_min_length": 5,
      "default": -1
    }
  ]
}
```
key | value
------------ | -------------
database | 数据库名
table | 表名
batch | 一条INSERT语句插入的行数
count| 每个工作线程迭代的轮数
process_num| 启动的进程数量
thread_num | 每个进程下启动的线程数量，`process_num*thread_num`为最终工作线程的数量
nullable_rate| 字段可以为NULL时，NULL值的比例，`(0-100)`代表百分比
columns | `可缺省` 如果缺省，根据从系统表中查询出的字段属性进行随机值生成
name | 列名
data_type | 字段数据类型
is_nullable | `可缺省` 0代表不可以为NULL，1代表可以为NULL
char_max_length | `可缺省` char类型的最大长度
char_min_length | `可缺省` char类型的最小长度
default | `可缺省` 指定值，支持`INT`, `CHAR`, `VARCHAR`, `TIMESTAMP`, CHAR如果default值过长将清除default属性，其余类型未设检查

**表结构限制:**

不支持`表达式`对分区进行定义

分区类型 | 说明
------------ | -------------
无分区 | 支持
一级HASH分区 | `INT`型分区键
一级RANGE分区 | `INT`型分区键
一级RANGE COLUMNS分区 | `INT`型分区键、`DATETIME`型分区键
二级分区RANGE COLUMNS与HASH组合 | HASH:`INT`型分区键、RANGE COLUMNS: `DATETIME`型分区键
>ddl文件中有建表语句例子

**字段数据类型限制:**

* `INT`
* `VARCHAR` `CHAR`
* `TIMESTAMP`   `DATETIME`  `DATE`  `YEAR`  `TIME`
* `DECIMAL`
* `DOUBLE`  `FLOAT`


